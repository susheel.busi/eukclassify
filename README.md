# EukClassify
# About

Classifying contigs as `prokaryote` or `eukaryote` based on ML classifiers

TODO: See `README_REPROD.md` for additional information on how to run the analysis on a different system than the Uni Luxembourg's HPC server.

# Setup

## Conda

[Conda user guide](https://docs.conda.io/projects/conda/en/latest/user-guide/index.html)

```bash
# install miniconda3
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
chmod u+x Miniconda3-latest-Linux-x86_64.sh
./Miniconda3-latest-Linux-x86_64.sh # follow the instructions
```

reate the main `snakemake` environment

```bash
# create venv
conda env create -f envs/requirements.yaml -n "YourEnvName"
```
