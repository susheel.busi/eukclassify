# Pipeline for running ANTISMASH on ROCK assemblies
#
# Example call: snakemake -s Snakefile --configfile config.yaml --use-conda --cores 1 -rpn

##############################
# MODULES
import os, re
import glob
import pandas as pd
import numpy as np
from Bio import SeqIO

##############################
# CONFIG
# can be overwritten by using --configfile <path to config> when calling snakemake
configfile:"config.yaml"

##############################
# Relevant directories
DATA_DIR = config["work_dir"]
RESULTS_DIR = config["results_dir"]
ENV_DIR = config["env_dir"]
SRC_DIR = config["scripts_dir"]
SUBMODULES = config["submodules_dir"]

##############################
# Input
# SAMPLES = [line.strip() for line in open("contig_length").readlines()]
NUMBER = config["extract"]["number"]
LENGTH = config["extract"]["length"]
KMER = config["extract"]["kmer"]
X = 1024	# number of columns in the create_contigs output files
MODELS = ["linsvm", "kersvm", "logreg", "ranfor", "neunet"]
EMBEDDINGS = ["pca", "kmeans", "none"]
TRANSFORMS = ["freq", "clr", "ilr"]

# localrules: 

##############################
# default
# NOTE: Adjust the rule all with the specific number/kmer/length profiles required

rule all:
    input:
        expand(os.path.join(RESULTS_DIR, "Counts_n10000_k{kmer}_s{length}.csv"), kmer=KMER, length=LENGTH),
##        expand(os.path.join(RESULTS_DIR, "Counts_n100000_k{kmer}_s10000.csv"), kmer=KMER)
##        expand(os.path.join(RESULTS_DIR, "grid_search/{type}_{trans}_10000_{kmer}_{length}.csv"), type=["SVC", "linSVC", "LogReg", "RF", "NN"], trans=["freq", "CLR"] + ['KmeansK=' + str(x) for x in np.arange(start=X//16, stop=X, step=X//16)], kmer=KMER, length=["10000"])
        expand(os.path.join(RESULTS_DIR, "grid_search/{model}_{embedding}_{transform}_10000_{kmer}_{length}.csv"), model=MODELS, embedding=EMBEDDINGS, transform=TRANSFORMS, kmer=KMER, length=["10000"]),
##        expand(os.path.join(RESULTS_DIR, "grid_search/{model}_{transform}_10000_{kmer}_{length}.csv"), model=MODELS, transform=['KmeansK=' + str(x) for x in np.arange(start=X//16, stop=X, step=X//16)], kmer=KMER, length=["10000"]),
        expand(os.path.join(RESULTS_DIR, "logs/bac_genome_decompress.done")),
        expand(os.path.join(RESULTS_DIR, "CAMISIM/mini_config_edited.ini")),
        expand(os.path.join(RESULTS_DIR, "CAMISIM/out"))

#########################################
# Rules for contig creation from RefSeq #
#########################################
rule create_contigs:
    output:
        os.path.join(RESULTS_DIR, "Counts_n{number}_k{kmer}_s{length}.csv")
    log:
        os.path.join(RESULTS_DIR, "logs/ContigExtraction.{number}.{kmer}.{length}.log")
    benchmark:
        os.path.join(RESULTS_DIR, "benchmarks/n{number}_k{kmer}_s{length}.benchmark.txt")
    message:
        "Extracting {wildcards.number} contigs with {wildcards.kmer}-mers and average length: {wildcards.length}"
    shell:
        "(date && python3 GetContigs.py -n {wildcards.number} -k {wildcards.kmer} -s {wildcards.length} && date) &> {log}"

rule test_dataset:
    input:
        KMER_file=os.path.join(RESULTS_DIR, "Counts_n{number}_k{kmer}_s{length}.csv")
    output:
        CSV=os.path.join(RESULTS_DIR, "test_data/eukclassify_n{number}_k{kmer}_s{length}_testdata.csv")
    log:
        os.path.join(RESULTS_DIR, "logs/test_n{number}_k{kmer}_s{length}_data.log")
    message:
        "Creating a subset of the input data to try out the scripts"
    run:
        df=pd.read_csv(input.KMER_file, header=0, index_col=[0,1])
        df_sub=df.sample(axis=1, n=126).sample(axis=0, n=256, random_state=43731)
        df_sub.sort_index(ascending=True, inplace=True)
        df_sub.to_csv(output.CSV, header=True, index=True)

rule tune_methods:
    input:
        os.path.join(RESULTS_DIR, "Counts_n{number}_k{kmer}_s{length}.csv")
#        rules.test_dataset.output.CSV
    output:
        os.path.join(RESULTS_DIR, "grid_search/{model}_{embedding}_{transform}_{number}_{kmer}_{length}.csv")
    log:
        os.path.join(RESULTS_DIR, "logs/Stats_{model}_{embedding}_{transform}_n{number}_k{kmer}_s{length}.log")
    threads:
        config["tuning"]["threads"]
    conda:
        os.path.join(ENV_DIR, "tuning.yaml")
    params:
        SEED=63,
        CV=5,
        VERB=2
    benchmark:
        os.path.join(RESULTS_DIR, "benchmarks/{model}_{embedding}_{transform}_{number}_{kmer}_{length}.benchmark.txt")
    message:
        "Tuning the different methods for {wildcards.model}, {wildcards.embedding} and {wildcards.number} contigs with {wildcards.kmer}-mers and average length: {wildcards.length}"
    script:
        "scripts/tune_methods.py"

rule tune_KM_methods:
    input:
        os.path.join(RESULTS_DIR, "Counts_n{number}_k{kmer}_s{length}.csv")
    output:
        os.path.join(RESULTS_DIR, "grid_search/KMEANS_{transform}_{number}_{kmer}_{length}.csv")
    log:
        os.path.join(RESULTS_DIR, "logs/KMEANS.Stats_{transform}_n{number}_k{kmer}_s{length}.log")
    threads: 1
    conda:
        os.path.join(ENV_DIR, "tuning.yaml")
    params:
        SEED=63,
        CV=5,
        VERB=2
    benchmark:
        os.path.join(RESULTS_DIR, "benchmarks/{transform}_{number}_{kmer}_{length}.benchmark.txt")
    message:
        "Tuning the different methods for KMEANS and {wildcards.number} contigs with {wildcards.kmer}-mers and average length: {wildcards.length}"
    script:
        "scripts/tune_KM.py"


############
# CAMISIM - Simulated datasets #
############
rule decompress_bac_genomes:
    output:
        touch(os.path.join(RESULTS_DIR, "logs/bac_genome_decompress.done"))
    log:
        os.path.join(RESULTS_DIR, "logs/decompress.log")
    message:
        "Decompressing bacterial genomes for CAMISIM"
    benchmark:
        os.path.join(RESULTS_DIR, "benchmarks/decompress.benchmark.txt")
    shell:
        "(date && scripts/decompress_bac_genome.sh && date) &> {log}"

rule copy_CAMISIM_config:
    input:
        os.path.join(SUBMODULES, "CAMISIM/defaults/mini_config.ini")
    output:
        os.path.join(RESULTS_DIR, "CAMISIM/mini_config.ini")
    log:
        os.path.join(RESULTS_DIR, "logs/copy_config.log")
    message:
        "Cloning CAMISIM repository"
    shell:
        """(date && cat {input} | sed 's/=out/={{odir}}/g' | sed 's/=defaults\/metadata\.tsv/={{metadata}}/g' | sed 's/=defaults\/genome_to_id\.tsv/={{genomes}}/g' | sed 's/max_processors=8/max_processors={{threads}}/g' | sed 's/genomes_total=24/genomes_total={{total_genomes}}/g' | sed 's/genomes_real=24/genomes_real={{real_genomes}}/g' > {output} && date) &> {log}"""

rule edit_config:
    input:
        config=os.path.join(RESULTS_DIR, "CAMISIM/mini_config.ini"),
        metadata=os.path.join(SUBMODULES, "updated_metadata.tsv"),
        genomes=os.path.join(SUBMODULES, "edited_genome_to_id.tsv")
    output:
        oconfig=os.path.join(RESULTS_DIR, "CAMISIM/mini_config_edited.ini")
    log:
        os.path.join(RESULTS_DIR, "logs/edit_config.log")
    params:
        threads=config["camisim"]["threads"],
        odir=directory(os.path.join(RESULTS_DIR, "CAMISIM/out/"))
    message:
        "Editing the config with the correct path variables"
    run:
        with open(input.config, 'r') as ifile, open(output.oconfig, 'w') as ofile:
            data = ifile.read()
            genome_rows=pd.read_csv(input.genomes, sep="\t", header=None).shape[0]

            data=data.format(odir=os.path.abspath(params.odir), metadata=input.metadata, 
                     genomes=input.genomes, threads=params.threads, 
                         total_genomes=genome_rows, real_genomes=genome_rows)

            ofile.write(data)
    
rule simulate_camisim_dataset:
    input:
        file=os.path.join(SUBMODULES, "CAMISIM/metagenomesimulation.py"),
        config=rules.edit_config.output.oconfig
    output:
        directory(os.path.join(RESULTS_DIR, "CAMISIM/out"))
    conda:
        os.path.join(ENV_DIR, "camisim.yaml")
    log:
        os.path.join(RESULTS_DIR, "logs/simulate_metaG.log")
    benchmark:
        os.path.join(RESULTS_DIR, "benchmarks/CAMISIM_simulate.benchmark.txt")
    message:
        "Generating a random metagenome with CAMISIM"
    shell:
        "(date && cd $(dirname {input.file}) && "
        "python {input.file} {input.config} && date) &> {log}"
