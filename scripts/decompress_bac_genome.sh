#!/bin/bash -l
# Following script will decompress the list of bacterial genomes for CAMISIM

cd /mnt/data/sbusi/fastq_NewZealand/databases/refseq/refseq/eukclassify

for file in $(sed '1d' camisim_bacteria_list | awk -F'\t' '{print $NF}')
do 
    echo $(basename "$file" | sed 's/_[^_]*//2g')
    gunzip -c $file > /mnt/data/sbusi/fastq_NewZealand/databases/refseq/refseq/curated_bac_genomes/$(basename $file | sed 's/_[^_]*//2g').fna
done
