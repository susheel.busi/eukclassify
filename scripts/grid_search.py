#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Functions to perform cross-validation grid searches.
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from helpers import *
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import balanced_accuracy_score
from sklearn.metrics import make_scorer
from sklearn.pipeline import Pipeline

from sklearn.svm import SVC
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.model_selection import RepeatedStratifiedKFold

def grid_search(X, y, model, emb_func, trans_func, param_grid, scorings, cv, cores, pipe_char, verbose=0):
    """
    Performs a cross validation grid search of the given model for the given parameter
    grid param_grid. It computes the global accuracy, as well as the accuracy of each class.
    The learning and prediction time of each method is also stored.

    Parameters
    ----------
    X, y: the datapoints and associated labels
    param_grid: dict, dictionnary with paramaeters names (str) as keys and lists of
        parameter setting to try as values
    model: the model to test
    trans_func: the function used for data transformation
    cv: int, number of cross-validation folds
    verbose: int, controls the verbosity: the higher, the more messages

    Returns
    -------
    df: panda DataFrame containing the cross-validation accuracies and time used to learn and predict
    """
    if pipe_char == 'freq_none':
        pipe = Pipeline(steps=[('Transformation', trans_func), ('Model', model)])
    elif pipe_char == 'clr_none':
        pipe = Pipeline(steps=[('Transformation', trans_func), ('Model', model)])
    elif pipe_char == 'ilr_none':
        pipe = Pipeline(steps=[('Transformation', trans_func), ('Model', model)])
    elif pipe_char == 'freq_pca':
        pipe = Pipeline(steps=[('Transformation', trans_func), ('Embedding', emb_func), ('Model', model)])
    elif pipe_char == 'clr_pca':
        pipe = Pipeline(steps=[('Transformation', trans_func), ('Embedding', emb_func), ('Model', model)])
    elif pipe_char == 'ilr_pca':
        pipe = Pipeline(steps=[('Transformation', trans_func), ('Embedding', emb_func), ('Model', model)])
    elif pipe_char == 'freq_kmeans':
        pipe = Pipeline(steps=[('Embedding', emb_func), ('Model', model)])
    elif pipe_char == 'clr_kmeans':
        pipe = Pipeline(steps=[('Embedding', emb_func), ('Transformation', trans_func), ('Model', model)])
    elif pipe_char == 'ilr_kmeans':
        pipe = Pipeline(steps=[('Embedding', emb_func), ('Transformation', trans_func), ('Model', model)])

    # perform the grid_search
    inner_cv = RepeatedStratifiedKFold(n_splits=3, n_repeats=10, random_state=24)
    grid_search = GridSearchCV(pipe, param_grid=param_grid, cv=inner_cv, refit='accuracy', n_jobs=cores, verbose=verbose)
    grid_search.fit(X, y)

    outer_cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=10, random_state=24)
    scores = cross_validate(grid_search, X, y, scoring=scorings, n_jobs=cores, cv=outer_cv)

    # store the result in a dataframe
    best_params = grid_search.best_params_
    print(best_params)
    print(scores)
    metrics = list(scorings.keys())
    res = dict()
    for param in best_params.keys():
        res[param] = best_params[param]
    for metric in metrics:
        res[metric] = scores[metric]
    df = pd.DataFrame.from_dict(res)
    return df


