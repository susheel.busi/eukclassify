#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Some useful functions to process the data, compute label accuracies
and plot grid search results.
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.metrics import confusion_matrix
from sklearn.metrics import balanced_accuracy_score
from sklearn.model_selection import cross_validate
from sklearn.cluster import KMeans
from sklearn.decomposition import PCA
from skbio.stats.composition import clr, ilr


def load_csv_data(data_path, n_min=1000):
    """Loads data and returns y (class labels), tX (features) and ids (event ids)"""
    print('Loading data...')
    y = np.genfromtxt(data_path, delimiter=",", skip_header=1, dtype=str, usecols=1)
    data = np.genfromtxt(data_path, delimiter=",", skip_header=1)
    ids = data[:, 0].astype(np.int)
    X = data[:, 2:]

    # convert class labels from strings to binary (0,1)
    yb = np.ones(len(y))
    yb[np.where(y=='Prokaryote')] = 0

    # Remove rows having less than n_min counts
    print('Removing rows with less than n_min counts...')
    to_delete = [i for i in range(X.shape[0]) if np.sum(X[i,]) < n_min]
    yb   = np.delete(yb,   to_delete, axis=0)
    ids = np.delete(ids, to_delete, axis=0)
    X   = np.delete(X,   to_delete, axis=0)

    print('Data loaded!')
    return yb, X, ids

def FREQ_transform(X):
    """Transforms the counts contained in the data X into frequencies."""
    return(X / X.sum(axis=1, keepdims=True))

def CLR_transform(X):
    """Applies the Centered Log-ratio Transformation to the data X."""
    #minval = np.min(X[np.nonzero(X)])
    #X[X == 0] = minval
    #X = np.log(X)
    #X = X - np.mean(X, axis = 0)
    freqX = FREQ_transform(X)
    freqX = freqX+1
    clrX = clr(freqX)
    return(clrX)

def ILR_transform(X):
    """Applies the Centered Log-ratio Transformation to the data X."""
    freqX = FREQ_transform(X)
    freqX = freqX+1
    ilrX = ilr(freqX)
    return(ilrX)

def create_kmeans_data(data, labels):
    """
    Creates a transformed data by clustering the features according to labels.
    """
    kmeans_trans_X = np.zeros((data.shape[0], len(set(labels))))

    labels_list = list(set(labels))
    for cluster_label in labels_list:
        cluster_cols = np.where(labels == cluster_label)[0]
        cluster_sums = np.sum(data[:, cluster_cols], axis=1)
        kmeans_trans_X[:,[cluster_label]] = np.expand_dims(cluster_sums, axis=1)
    return kmeans_trans_X

def PCA_embedding(X, n):
    pca = PCA(n_components=n)
#    X_freq = FREQ_transform(X)
    return pca.fit_transform(X)

def KMEANS_embedding(X, km):
    """Transforms the counts contained in the data X into K-means clustered frequencies."""
    X_freq = FREQ_transform(X)
    kmeans = KMeans(n_clusters=km, random_state=24).fit(X_freq.T)
    kX = create_kmeans_data(X_freq, kmeans.labels_)
    return kX

def NO_embedding(X):
    return X

def euk_accuracy(y_test, y_pred):
    """Computes the accuracy for the class 'eukaryote' only."""
    matrix = confusion_matrix(y_test, y_pred)
    class_ac = matrix.diagonal() / matrix.sum(axis=1)
    return class_ac[1]

def pro_accuracy(y_test, y_pred):
    """Computes the accuracy for the class 'prokaryote' only."""
    matrix = confusion_matrix(y_test, y_pred)
    class_ac = matrix.diagonal() / matrix.sum(axis=1)
    return class_ac[0]

