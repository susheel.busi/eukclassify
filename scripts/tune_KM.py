#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Script used to do a grid search over hyperparameters of different sklerarn methods:
– linear SVC : tune the penalization parameter C
– kernel SVM : tune the penalization parameter C and the kernel coefficient gamma
– logistic regression : tune the penalization parameter C
– random forest : tune the maximum depth and the forest size
– neural network : tune the number of nodes in the hidden layer
For each method, we use the dataset transformation chosen in the previous step.
"""

##########
# IMPORT #
##########
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from helpers import *
from grid_search import *

##################################################
# LOGGING
##################################################
# logger
logging.basicConfig(
    filename=str(snakemake.log),
    filemode="w",
    level=logging.DEBUG,
    format='[%(asctime)s] %(name)s %(levelname)s: %(message)s'
)
logger = logging.getLogger(__file__)


#######################################
y, X, _ = load_csv_data(snakemake.input[0])
X_freq = FREQ_transform(X)
X_clr = CLR_transform(X_freq)

k_list = np.arange(start=X.shape[1]//16, stop=X.shape[1], step=X.shape[1]//16)
#######################################

##########
# PARAMS #
##########
seed = snakemake.params.SEED
cv = snakemake.params.CV
verbose = snakemake.params.VERB
n = str(snakemake.wildcards.number)
k = str(snakemake.wildcards.kmer)
s = str(snakemake.wildcards.length)

# define the models to test
models_dict = {'lin_svm': LinearSVC(random_state=seed, max_iter=10000),
               'ker_svm': SVC(random_state=seed),
               'log_reg': LogisticRegression(max_iter=10000, random_state=seed),
               'ran_for': RandomForestClassifier(random_state=seed),
               'neu_net': MLPClassifier(random_state=seed, solver='adam', max_iter=500)}

# define the parameters to test
parameters_dict = {'lin_svm': {'C': np.logspace(-2, 3, 20)},
                   'ker_svm': {'C': [0.01, 0.1, 10, 100], 'gamma':[0.001, 0.01, 0.1, 1, 10, 100]},
                   'log_reg': {'C': np.logspace(0, 3, 20)},
                   'ran_for': {'n_estimators': [20, 80, 100, 150, 200], 'max_depth':[5, 10, 15, 20, 35, 50]},
                   'neu_net':  {'hidden_layer_sizes': [(50,), (100,), (150,), (200,), (250,), (300,), (350,), (400,), (450,), (500,)]}}

# define the transformations to test
trans_dict = {kmeans: KMEANS_transform()}

# define the scoring functions
scorings = {'accuracy': make_scorer(balanced_accuracy_score),
            'eukaryote_accuracy':make_scorer(euk_accuracy),
            'prokaryote_accuracy':make_scorer(pro_accuracy)}

model = models_dict[snakemake.wildcards.model]
param_grid = parameters_dict[snakemake.wildcards.model]
trans_func = trans_dict[snakemake.wildcards.transform]

results = grid_search(X, y, model, trans_func, param_grid, cv, verbose)
results.to_csv(snakemake@output[0]. index=False)
    

## CLR transformation
#grid_search_linSVC(X_clr, y,'_'.join(['linSVC','CLR',n,k,s]), cv, seed, verbose)
#grid_search_SVC(X_clr, y,'_'.join(['SVC','CLR',n,k,s]), cv, seed, verbose)
#grid_search_LogReg(X_clr, y,'_'.join(['LogReg','CLR',n,k,s]), cv, seed, verbose)
#grid_search_RF(X_clr, y,'_'.join(['RF','CLR',n,k,s]), cv, seed, verbose)
#grid_search_NN(X_clr, y,'_'.join(['NN','CLR',n,k,s]), cv, seed,  verbose)

## KMEANS trans
#for km in k_list:
#    print(k)
#    kmeans = KMeans(n_clusters=km, random_state=seed).fit(X_freq.T)
#    kX = create_kmeans_data(X_freq, kmeans.labels_)
#    grid_search_linSVC(kX, y,'_'.join(['linSVC','KmeansK='.format(km),n,k,s]), cv, seed, verbose)
#    grid_search_SVC(kX, y,'_'.join(['SVC','KmeansK='.format(km),n,k,s]), cv, seed, verbose)
#    grid_search_LogReg(kX, y,'_'.join(['LogReg','KmeansK='.format(km),n,k,s]), cv, seed, verbose)
#    grid_search_RF(kX, y,'_'.join(['RF','KmeansK='.format(km),n,k,s]), cv, seed, verbose)
#    grid_search_NN(kX, y,'_'.join(['NN','KmeansK='.format(km),n,k,s]), cv, seed, verbose)
