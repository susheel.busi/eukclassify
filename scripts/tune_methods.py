#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Script used to do a grid search over hyperparameters of different sklerarn methods:
– linear SVC : tune the penalization parameter C
– kernel SVM : tune the penalization parameter C and the kernel coefficient gamma
– logistic regression : tune the penalization parameter C
– random forest : tune the maximum depth and the forest size
– neural network : tune the number of nodes in the hidden layer
For each method, we use the dataset transformation chosen in the previous step.
"""

##########
# IMPORT #
##########
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from helpers import *
from grid_search import *
from sklearn.metrics import f1_score
from sklearn.decomposition import PCA
from sklearn.metrics import roc_auc_score
from sklearn.preprocessing import FunctionTransformer

##################################################
# LOGGING
##################################################
# logger
logging.basicConfig(
    filename=str(snakemake.log),
    filemode="w",
    level=logging.DEBUG,
    format='[%(asctime)s] %(name)s %(levelname)s: %(message)s'
)
logger = logging.getLogger(__file__)


#######################################
y, X, _ = load_csv_data(snakemake.input[0])
#k_list = np.arange(start=X.shape[1]//16, stop=X.shape[1], step=X.shape[1]//16)
#######################################

##########
# PARAMS #
##########
seed = snakemake.params.SEED
cv = snakemake.params.CV
verbose = snakemake.params.VERB
n = str(snakemake.wildcards.number)
k = str(snakemake.wildcards.kmer)
s = str(snakemake.wildcards.length)

# define the models to test
models_dict = {'linsvm': LinearSVC(random_state=seed, max_iter=10000),
               'kersvm': SVC(random_state=seed),
               'logreg': LogisticRegression(max_iter=10000, random_state=seed),
               'ranfor': RandomForestClassifier(random_state=seed),
               'neunet': MLPClassifier(random_state=seed, solver='adam', max_iter=500)}

# define the parameters to test
parameters_dict = {'linsvm': {'Model__C': np.logspace(-2, 3, 20)},
                   'kersvm': {'Model__C': [0.01, 0.1, 10, 100], 'Model__gamma':[0.001, 0.01, 0.1, 1, 10, 100]},
                   'logreg': {'Model__C': np.logspace(0, 3, 20)},
                   'ranfor': {'Model__n_estimators': [20, 80, 100, 150, 200], 'Model__max_depth':[5, 10, 15, 20, 35, 50]},
                   'neunet': {'Model__hidden_layer_sizes': [(50,), (100,), (150,), (200,), (250,), (300,), (350,), (400,), (450,), (500,)]}}

steps_emb = np.arange(start=X.shape[1]//16, stop=X.shape[1], step=X.shape[1]//16)

# define the transformations to test
trans_dict = {'freq': FunctionTransformer(FREQ_transform, validate=True),
              'clr': FunctionTransformer(CLR_transform, validate=True),
              'ilr': FunctionTransformer(ILR_transform, validate=True) }

# define the scoring functions
scorings = {'accuracy': make_scorer(balanced_accuracy_score),
            'eukaryote_accuracy':make_scorer(euk_accuracy),
            'prokaryote_accuracy':make_scorer(pro_accuracy),
            'auc': make_scorer(roc_auc_score),
            'f1_score': make_scorer(f1_score)}

if snakemake.wildcards.embedding == 'none':
    embedding_dict = {'none': FunctionTransformer(NO_embedding, validate=True)}
    model = models_dict[snakemake.wildcards.model]
    print(models_dict)
    param_grid = parameters_dict[snakemake.wildcards.model]
    trans_func = trans_dict[snakemake.wildcards.transform]
    emb_func = embedding_dict[snakemake.wildcards.embedding]
    cores = snakemake.threads
    pipe_char = "_".join([snakemake.wildcards.transform, snakemake.wildcards.embedding])
    results = grid_search(X, y, model, emb_func, trans_func, param_grid, scorings, cv, cores, pipe_char, verbose)
    results.to_csv(snakemake.output[0], index=False)
else:
    all_results = pd.DataFrame()
    for i in steps_emb:
        embedding_dict = {'pca': FunctionTransformer(PCA_embedding, kw_args={'n':i}, validate=True),
                       'kmeans': FunctionTransformer(KMEANS_embedding, kw_args={'km':i}, validate=True)}
        model = models_dict[snakemake.wildcards.model]
        print(models_dict)
        param_grid = parameters_dict[snakemake.wildcards.model]
        trans_func = trans_dict[snakemake.wildcards.transform]
        emb_func = embedding_dict[snakemake.wildcards.embedding]
        cores = snakemake.threads
        pipe_char = "_".join([snakemake.wildcards.transform, snakemake.wildcards.embedding])

        results = grid_search(X, y, model, emb_func, trans_func, param_grid, scorings, cv, cores, pipe_char, verbose)
        results['k'] = i
        all_results = all_results.append(results)
    all_results.to_csv(snakemake.output[0], index=False)
        
